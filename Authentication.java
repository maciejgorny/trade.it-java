/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model;

import com.gorny.trading.model.response.GenericResponse;

/**
 *
 * @author maciejg
 */
public abstract class Authentication extends GenericResponse {
    public abstract String getUserId();
}
