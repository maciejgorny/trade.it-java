/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model;

import com.gorny.trading.Broker;
import com.gorny.trading.Callable;

/**
 *
 * @author maciejg
 */
public interface ITask extends Callable {
    
    void execute(Object... params);
        
}
