/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.response;

import com.gorny.trading.model.Authorization;

/**
 * Trade.it
 * @author maciejg
 * 
 */
public class AuthorizeResponse extends Authorization {

    public String userToken;
    public String userId;
    
    public String getUserId() {
        return userId;
    }
    public String getToken() {
        return token;
    }
    public String getUserToken() {
        return userToken;
    }
}
