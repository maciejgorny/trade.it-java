/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.response;

import java.util.List;

/**
 * Trade.it
 * Standard error handling fields
 * @author maciejg
 * 
 *  Error codes:
    100	System Error
    200	Broker Execution Error - User should modify the input for the trade request
    300	Broker Authentication Error - Authentication info is incorrect or the user may have changed their login information and the oAuth token is no longer valid.
    400	Broker Account Error - User credentials are valid, but needs to take action on the brokers site (ie. sign exchange agreement, sign margin agreement)
    500	Params Error - Publisher should check the parameters being passed in
    600	Session Expired - Publisher should call authenticate again in order to generate a new session token
    700	Token invalid or expired - Publisher should call oAuthUpdate in order to refresh the token
 */
public class GenericResponse {
    /**
     * error code
     */
    public String code;
    
    /**
     * SUCCESS or ERROR
     */
    public String status;	
    
    /**
     * Token used for all subsequent user session requests.
     */
    public String token	;
    
    /**
     * i.e.: "Account Overview successfully fetched"
     */
    public String shortMessage; 	
    
    /**
     * Additional messages for the user
     */
    public List<String> longMessages;
    
    /**
     * 
     */
    public List<String> errorFields;
    
    /**
     * 
     */
    public String getToken(){
        return token;
    }
}
