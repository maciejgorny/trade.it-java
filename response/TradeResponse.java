/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gorny.trading.model.response.GenericResponse;
import com.gorny.trading.model.tradeit.OrderInfo;

/**
 * Trade.it
 * @author maciejg
 
 {
	"accountBaseCurrency": "USD",
	"broker": "Fidelity",
	"confirmationMessage": "Your order message H06BGSBB to buy 100 shares of S at $5.25 has been successfully transmitted to Fidelity at 06/08/16 6:25 PM EDT.",
	"longMessages": ["Transmitted successfully to Fidelity"],
	"orderInfo": {
		"universalOrderInfo": {
			"action": "buy",
			"quantity": 100,
			"symbol": "S",
			"price": {
				"type": "limit",
				"limitPrice": 5.25
			},
			"expiration": "gtc"
		},
		"action": "buy",
		"quantity": 100,
		"symbol": "S",
		"price": {
			"type": "limit",
			"limitPrice": 5.25,
			"last": 6.17,
			"ask": 6.16,
			"bid": 6.14,
			"timestamp": "08/05/2016 4:01:40pm"
		},
		"expiration": "gtc",
		"gtdDate": null,
		"specialHandling": null
	},
	"orderNumber": "H06BGSBB",
	"shortMessage": "Order Successfully Submitted",
	"status": "SUCCESS",
	"timestamp": "06/08/16 6:25 PM EDT",
	"token": "320c6305128c4739ba2b24a979a5764a",
        "utilsService":null
}
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TradeResponse extends GenericResponse {
    
    /**
     * The broker used to execute the trade
     */
    public String broker;
    
    /**
     * Display friendly confirmation order message
     */
    public String confirmationMessage;
    
    /**
     * Order number from the broker
     */
    public String orderNumber;
    
    /**
     * The timestamp of when the order was submitted
     */
    public String timestamp;
    
    /**
     * Order Info Object, contains details about the order that was place
     */
    public OrderInfo orderInfo; 
    
    /**
     * Base currency
     */
    public String accountBaseCurrency;
    
    /**
     * [no idea]
     */
    public String utilsService;
}
