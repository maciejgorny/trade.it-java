/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.response;

import com.gorny.trading.model.Authentication;
import com.gorny.trading.model.tradeit.Account;
import java.util.List;

/**
 * Trade.it
 * @author maciejg
 * 
 */
public class AuthenticateResponse extends Authentication {
    
    public String userToken;
    public String userId;
    public String accountNumber;
    public String name;
    public List<Account> accounts;
    
    public String getUserId(){
        return userId;
    }
}
