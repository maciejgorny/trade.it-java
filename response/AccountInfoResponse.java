/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Trade.it
 * @author maciejg
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountInfoResponse extends GenericResponse {
    
    public double availableCash;        //	Cash available to withdraw
    public double buyingPower;          //	The buying power of the account
    public double totalValue;           //	The total account value
    public double dayAbsoluteReturn;    //	The daily return of the account
    public double dayPercentReturn;     //	The daily return percentage
    public double totalAbsoluteReturn;  //	The total absolute return on the account
    public double totalPercentReturn;   //	The total percentage return on the account    
    public String accountBaseCurrency;
    
    public double getAvailableCash() {
        return availableCash;
    }
    public double getTotalValue() {
        return totalValue;
    }
    public double getDayPercentReturn() {
        return dayPercentReturn;
    }
}
