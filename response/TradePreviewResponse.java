/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gorny.trading.model.tradeit.OrderDetails;
import java.util.List;

/**
 * Trade.it
 * @author maciejg
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TradePreviewResponse extends GenericResponse {

    /**
     * TradeIt internal orderId used in place order request
     */
    public String orderId;

    /**
     * Array of strings. Passive warnings to be shown to the user
     */
    public List<String> warningsList;
    
    /**
     * These warnings must be presented to the user and acknowledged via some sort of checkbox/switch/popup
     */
    public List<String> ackWarningsList;	
    
    /**
     * Order details object
     */
    public OrderDetails orderDetails;
    
    /**
     * 
     */
    public String accountBaseCurrency;
}
