/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gorny.trading.model.tradeit.Position;
import java.util.List;

/**
 * Trade.it
 * @author maciejg
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PositionResponse extends GenericResponse {
    
    /**
     * Indicates the page of a multi page result
     */
    public int currentPage;

    /**
     * The total number of pages to retrieve all positions
     */
    public int totalPages;

    /**
     * Array of position objects
     */
    List<Position> positions;
}
