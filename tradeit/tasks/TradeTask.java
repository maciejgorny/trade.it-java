/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit.tasks;

import com.gorny.trading.Callable;
import com.gorny.trading.model.Authentication;
import com.gorny.trading.model.TaskImpl;
import com.gorny.trading.model.tradeit.Request;
import com.gorny.trading.model.tradeit.RequestFactory;
import com.gorny.trading.model.response.TradeResponse;
import java.util.HashMap;

/**
 *
 * @author maciejg
 */
public class TradeTask extends TaskImpl {
    HashMap<String,String> tradeParams;
    
    public TradeTask() {
        super();
    }
    
    public TradeTask(Callable callWhenDone) {
        super(callWhenDone);
    }
    
    @Override
    public void execute(Object... params) {        
        
        Authentication authentication = (Authentication) params[0];
        String orderId = (String) params[1];
        
        tradeParams = new HashMap<String, String>();
        tradeParams.put("token", authentication.getToken());
        tradeParams.put("orderId", orderId);
        
        Request request = RequestFactory.getBuyLimitRequest(tradeParams);
        request.make(success, fail, TradeResponse.class);
    }
    
    private Callable success = new Callable() {
        @Override
        public void call(Object... params) {
            if (finishedCallback != null)
                finishedCallback.call(Boolean.TRUE, params[0], tradeParams);
        }
    };
    
    private Callable fail = new Callable() {
        @Override
        public void call(Object... params) {
            if (finishedCallback != null)
                finishedCallback.call(Boolean.FALSE, params[0], tradeParams);
        }
    };
}
