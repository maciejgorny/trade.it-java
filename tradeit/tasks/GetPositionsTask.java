/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit.tasks;

import com.gorny.trading.Callable;
import com.gorny.trading.model.TaskImpl;
import com.gorny.trading.model.Authentication;
import com.gorny.trading.model.response.TradeResponse;
import com.gorny.trading.model.tradeit.Request;
import com.gorny.trading.model.tradeit.RequestFactory;

/**
 *
 * @author maciejg
 */
public class GetPositionsTask extends TaskImpl {
    
    public GetPositionsTask() {
        super();
    }
    
    public GetPositionsTask(Callable callWhenDone) {
        super(callWhenDone);
    }
    
    @Override
    public void execute(Object... params) {        
        
        Authentication authentication = (Authentication) params[0];
        String accountNumber = (String) params[1];
        int page = Integer.parseInt((String)params[1]);
        
        Request request = RequestFactory.getPositionsRequest(authentication, accountNumber, page);
        request.make(success, fail, TradeResponse.class);
    }
    
    private Callable success = new Callable() {
        @Override
        public void call(Object... params) {
            if (finishedCallback != null)
                finishedCallback.call(Boolean.TRUE, params[0]);
        }
    };
    
    private Callable fail = new Callable() {
        @Override
        public void call(Object... params) {
            if (finishedCallback != null)
                finishedCallback.call(Boolean.FALSE, params[0]);
        }
    };
}
