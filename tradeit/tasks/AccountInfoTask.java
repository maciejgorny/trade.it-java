/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit.tasks;

import com.gorny.trading.Callable;
import com.gorny.trading.model.Authentication;
import com.gorny.trading.model.TaskImpl;
import com.gorny.trading.model.response.AccountInfoResponse;
import com.gorny.trading.model.tradeit.Request;
import com.gorny.trading.model.tradeit.RequestFactory;

/**
 *
 * @author maciejg
 */
public class AccountInfoTask extends TaskImpl { 

    public AccountInfoTask() {    
        super();
    }
    
    public AccountInfoTask(Callable done) {
        super(done);
    }
    
    @Override
    public void execute(Object... params) {
        
        Authentication authentication = (Authentication) params[0];
        String accountNumber = (String) params[1];
        
        Request request = RequestFactory.getAccountInfoRequest(accountNumber, authentication);
        request.make(
                /*success response*/
                new Callable() {
                    @Override
                    public void call(Object... params) {                        
                        if (finishedCallback != null)
                            finishedCallback.call(Boolean.TRUE, params[0]);
                    }
                }, 
                
                /*error response*/
                new Callable() {
                    @Override
                    public void call(Object... params) {                        
                        if (finishedCallback != null) {
                            finishedCallback.call(Boolean.FALSE, params[0]);
                        }
                        
                    }
                }, AccountInfoResponse.class);
    }
}
