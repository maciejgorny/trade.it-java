/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit.tasks;

import com.gorny.trading.Callable;
import com.gorny.trading.model.TaskImpl;
import com.gorny.trading.model.tradeit.Request;
import com.gorny.trading.model.tradeit.RequestFactory;
import com.gorny.trading.model.response.TradePreviewResponse;
import java.util.HashMap;

/**
 *
 * @author maciejg
 */
public class TradePreviewTask extends TaskImpl {
    
    public TradePreviewTask() {
        super();
    }
    
    public TradePreviewTask(Callable callWhenDone) {
        super(callWhenDone);
    }
    
    @Override
    public void execute(Object... params) {
               
        final HashMap<String,String> orderParams = (HashMap) params[0];        
        Request request = RequestFactory.getPreviewTradeRequest(orderParams);
        request.make(
                /*success response*/
                new Callable() {
                    @Override
                    public void call(Object... params) {                        
                        if (finishedCallback != null)
                            finishedCallback.call(Boolean.TRUE, params[0], orderParams);
                    }
                }, 
                
                /*error response*/
                new Callable() {
                    @Override
                    public void call(Object... params) {                        
                        if (finishedCallback != null) {
                            finishedCallback.call(Boolean.FALSE, params[0], orderParams);
                        }
                        
                    }
                }, TradePreviewResponse.class);
    }
    
}
