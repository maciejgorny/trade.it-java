/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit;

/**
 *
 * @author maciejg
 */
public class Account {
    private String accountNumber;
    private String name;
    private String accountBaseCurrency;
    private boolean tradable;

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountBaseCurrency() {
        return accountBaseCurrency;
    }

    public void setAccountBaseCurrency(String accountBaseCurrency) {
        this.accountBaseCurrency = accountBaseCurrency;
    }

    public boolean isTradable() {
        return tradable;
    }

    public void setTradable(boolean tradable) {
        this.tradable = tradable;
    }
    
}
