/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit;

/**
 *
 * @author maciejg
 */
public class UniversalOrderInfo {
        
    /**
     * The street symbol for the trade
     */
    public String symbol;
    
    /**
     * The order action used for the trade "buy", "sell", "sellShort", "buyToCover"
     */
    public String action;
    
    /**
     * The quantity of the security traded
     */
    public double quantity;
    
    /**
     * "day" or "gtc"
     */
    public String expiration;
    
    /**
     * Object, price information
     */
    public Price price;
    
}
