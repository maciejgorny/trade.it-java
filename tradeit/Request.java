/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit;

import com.gorny.trading.Callable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import javax.net.ssl.HttpsURLConnection;

/**
 *
 * @author maciejg
 */
public abstract class Request<T> {
    
    public static String SUCCESS = "Success";
    public static String ERROR   = "Error";
    public static String FAILED  = "Failure";
    
    public abstract void make(Callable success, Callable error, Class<T> responseType);
    
    protected String endpoint = null;
    protected HashMap<String,String> parameters = null;
    
    public void endpoint(String url){
        this.endpoint = url;
    };
    
    public void parameters(HashMap<String,String> params){
        this.parameters = params;
    };
    
    protected void httpPost(Callable callback) {
        BufferedReader in = null;
        String inputLine = null;
        StringBuilder response = new StringBuilder();
        try {
            URL url = new URL(endpoint);
            System.out.println(" ** [httPost] "+endpoint);
            
            StringBuilder postData = new StringBuilder("{");
            if (parameters != null && !parameters.isEmpty()) {
                for (String key : parameters.keySet()) {
                    if (postData.length()>1)
                        postData.append(",");
                    postData.append("\"").append(key).append("\"")
                            .append(":")
                            .append("\"").append(parameters.get(key)).append("\"");
                }
            }
            postData.append("}");
            System.out.println(" ** [httPost] data="+postData.toString());
            
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);

            in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine).append("\n");
            }
            if (callback != null)
                callback.call(SUCCESS, response.toString());
        
        } catch(Exception e) {
            e.printStackTrace();
            if (callback != null)
                callback.call(ERROR, e.getLocalizedMessage());
        } finally {
            try {
                if (in != null)
                    in.close();
            } catch (IOException ioe) { }
        }
    }
    
    protected void httpGet(Callable callback) {
        StringBuilder data = new StringBuilder();
        try {
            URL url = new URL(endpoint);
            URLConnection connection = url.openConnection();
            if (connection != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader((InputStream)connection.getContent()));
                String line = null;
                while ((line = reader.readLine())!=null) {                        
                    data.append(line).append("\n");
                }
                reader.close();
            } else {
                data.append("ERROR_SERVICE_NOT_REACHABLE");
            }
        } catch (IOException e) {
            data.append("ERROR_BAD_PARAMETERS");
        }
        if (callback != null) 
            callback.call(data.toString());
    }
}
