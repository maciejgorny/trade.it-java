/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit;

import com.gorny.trading.model.response.AuthorizeResponse;
import com.gorny.trading.model.Authorization;

import com.gorny.trading.Utils;
import com.gorny.trading.model.Authentication;
import java.util.HashMap;

/**
 *
 * @author maciejg
 */
public class RequestFactory {
    
    public static final String QA_KEY = "e61dd8870a714e96b7d3cb45da350ff1"; 
    public static final String PROD_KEY = "ecef2f6720304935bdce35d19df862a1";
    
    public static final String QA_HOST = "https://ems.qa.tradingticket.com";
    public static final String PROD_HOST = "https://ems.tradingticket.com";
    
    public static boolean isQA = false;
    
    public static Request getOAuthRequest() {
        /**
         * Create a new request instance.
         */
        Request request = new TradeItRequest();
        
        /**
         * Set the endpoint.
         */
        StringBuilder b = new StringBuilder(isQA ? QA_HOST : PROD_HOST);
        b.append("/api/v1/user/oAuthLink");
        request.endpoint(b.toString());
        
        /**
         * Set the POST parameters.
         * TODO: these params should come from config settings.
         */
        HashMap<String,String> postParams = new HashMap<String, String>();
        postParams.put("apiKey", isQA ? QA_KEY : PROD_KEY);
        /*
        username:           Response:
        dummy               no errors
        dummyNotMargin      returns error response if request is to place a sell short or buy to cover
        dummyNull           returns null values for every field that can potentially return as null
        dummySecurity       returns security question response (answer is tradingticket)
        dummySecurityImage  returns response with challenge image (mainly used for IB)
        dummyOptionLong     returns response with multiple options for the security question answer (answer is option1)
        any other ID that is not dummy	returns error response with authentication error

        When username is dummy or dummySecurity:

        Order Size:                     Returns:
        quantity is below 50            returns review response with no warning messages
        quantity is between 50 and 99   returns review response with warnings and ack messages
        quantity is 100 and up          returns error response
         */
        if (isQA) {
            postParams.put("id", "dummy");
            postParams.put("password", "pass");
            postParams.put("broker", "Dummy");
        } else {
            postParams.put("id", "maciejg");
            postParams.put("password", "Bzvqbq_;");
            postParams.put("broker", "Fidelity");
        }        
        request.parameters(postParams);        
        return request; 
    }
    public static Request getOAuthUpdateRequest(Authorization originalAuth) {
        /**
         * Create a new request instance.
         */
        Request request = new TradeItRequest();
        
        /**
         * Set the endpoint.
         */
        StringBuilder b = new StringBuilder(isQA ? QA_HOST : PROD_HOST);
        b.append("/api/v1/user/oAuthUpdate");
        request.endpoint(b.toString());
        
        /**
         * Set the POST parameters.
         * TODO: these params should come from config settings.
         */
        HashMap<String,String> postParams = new HashMap<String, String>();
        if (isQA) {
            postParams.put("broker", "Dummy");
            postParams.put("id", "dummy");
            postParams.put("password", "pass");
            postParams.put("apiKey", QA_KEY);
            postParams.put("userId", originalAuth.getUserId());
        } else {
            postParams.put("broker", "Fidelity");
            postParams.put("id", "maciejg");
            postParams.put("password", "Bzvqbq_;");
            postParams.put("apiKey", PROD_KEY);
            postParams.put("userId", originalAuth.getUserId());
        }
        
        request.parameters(postParams);        
        return request; 
    }
    
    public static Request getAuthenticateRequest(AuthorizeResponse authorization) {
        
        Request request = new TradeItRequest();
        
        StringBuilder b = new StringBuilder(isQA ? QA_HOST : PROD_HOST);
        b.append("/api/v1/user/authenticate");
        request.endpoint(b.toString());
        
        HashMap<String,String> postParams = new HashMap<String, String>();   
        postParams.put("userToken", authorization.userToken);
        postParams.put("userId", authorization.userId);
        postParams.put("apiKey", isQA ? QA_KEY : PROD_KEY);    
        postParams.put("srv", String.valueOf(Utils.getTimestamp()));
        
        request.parameters(postParams);        
        return request; 
    }
    
    public static Request getAccountInfoRequest(String accountNumber, Authentication auth) {
        
        Request request = new TradeItRequest();
        
        StringBuilder b = new StringBuilder(isQA ? QA_HOST : PROD_HOST);
        b.append("/api/v1/balance/getAccountOverview");
        request.endpoint(b.toString());
        
        HashMap<String,String> postParams = new HashMap<String, String>();
        postParams.put("token", auth.getToken());      
        postParams.put("accountNumber", accountNumber);
        
        request.parameters(postParams);
        return request;
    }
    
    public static Request getPreviewTradeRequest(HashMap<String,String> orderParams) {
        
        Request request = new TradeItRequest();
        
        StringBuilder b = new StringBuilder(isQA ? QA_HOST : PROD_HOST);
        b.append("/api/v1/order/previewStockOrEtfOrder");
        request.endpoint(b.toString());
        
        request.parameters(orderParams);
        return request;
    }
    
    public static Request getBuyLimitRequest(HashMap<String,String> tradeParams) {
        
        Request request = new TradeItRequest();
        
        StringBuilder b = new StringBuilder(isQA ? QA_HOST : PROD_HOST);
        b.append("/api/v1/order/placeStockOrEtfOrder");
        request.endpoint(b.toString());
                        
        request.parameters(tradeParams);
        return request;
    }
    
    public static Request getPositionsRequest(Authentication authentication, String accountNumber, int page) {
        
        Request request = new TradeItRequest();
        
        StringBuilder b = new StringBuilder(isQA ? QA_HOST : PROD_HOST);
        b.append("/api/v1/position/getPositions");
        request.endpoint(b.toString());
                        
        HashMap<String,String> postParams = new HashMap<String, String>();
        postParams.put("token", authentication.getToken());      
        postParams.put("accountNumber", accountNumber);     
        postParams.put("page", String.valueOf(page));
        
        request.parameters(postParams);
        return request;
    }
    
    public static Request get_ANY_Request(Authentication authentication, String accountNumber) {
        
        Request request = new TradeItRequest();
        
        StringBuilder b = new StringBuilder(isQA ? QA_HOST : PROD_HOST);
        b.append("/api/v1/");
        request.endpoint(b.toString());
        
        HashMap<String,String> postParams = new HashMap<String, String>();
        postParams.put("token", authentication.getToken());      
        postParams.put("accountNumber", accountNumber);
        
        request.parameters(postParams);
        return request;
    }
    
}
