package com.gorny.trading.model.tradeit;

import com.gorny.trading.model.response.TradeResponse;
import com.gorny.trading.model.response.AuthorizeResponse;
import com.gorny.trading.model.response.GenericResponse;
import com.gorny.trading.model.response.AuthenticateResponse;
import com.gorny.trading.model.response.TradePreviewResponse;
import com.gorny.trading.model.response.AccountInfoResponse;
import com.gorny.trading.model.tradeit.tasks.TradeTask;
import java.util.List;
import java.util.ArrayList;

import com.gorny.trading.Broker;
import com.gorny.trading.Callable;
import com.gorny.trading.Logger;
import com.gorny.trading.constants.Error;
import com.gorny.trading.model.ITask;
import com.gorny.trading.model.response.PositionResponse;
import static com.gorny.trading.model.tradeit.RequestFactory.PROD_KEY;
import static com.gorny.trading.model.tradeit.RequestFactory.QA_KEY;
import static com.gorny.trading.model.tradeit.RequestFactory.isQA;
import com.gorny.trading.model.tradeit.tasks.AccountInfoTask;
import com.gorny.trading.model.tradeit.tasks.GetPositionsTask;
import com.gorny.trading.model.tradeit.tasks.TradePreviewTask;
import java.util.HashMap;

/**
 * http://www.trade.it implementation
 *
 * @author maciejg
 */
public class TradeIt extends Broker {

    public AuthorizeResponse    authorization = null;
    public AuthenticateResponse authentication = null;
    public AccountInfoResponse  accountInfo = null;
    
    private String accountNumber = null;    
    public Logger logger = new Logger();
    
    public String getName() {
        return "Trade.it";
    }
    
    public double getTransactionFee() {
        return BROKER_TRANSACTION_FEE;
    }
    
    public String getAccountNumber() {
        return accountNumber;
    }
    
    public TradeIt() {
        logger.initialize("tradeit");
    }
    
    @Override
    public void executeTrade(TradeType ticketType, double price, int quantity, String symbol) {
        super.executeTrade(ticketType, price, quantity, symbol);
    }
    
    public void buyMarket(String symbol, int quantity, Callable callback) {
    }

    public void sellMarket(String symbol, int quantity, Callable callback) {
    }

    /**
     * API requires that a preview call be executed first (TradePreviewTask), 
     * and only then followed by the trade call (TradeTask)
     * 
     * @param symbol
     * @param price
     * @param quantity
     * @param callback 
     */
    public void buyLimit(String symbol, double price, int quantity, final Callable callback) {
        logger.log("TradeIt -> buyLimit");
        
        if (!isAuthorized()) {
            callback.call(Boolean.FALSE, Error.NOT_AUTHORIZED);
            return;
        }
        
        if (!isAuthenticated()) {
            callback.call(Boolean.FALSE, Error.NOT_AUTHENTICATED);
            return;
        }
        
        final HashMap<String,String> orderParams = new HashMap<String, String>();
        orderParams.put("apiKey", isQA ? QA_KEY : PROD_KEY);
        orderParams.put("token",            authentication.getToken());
        orderParams.put("accountNumber",    accountNumber);
        orderParams.put("orderAction",      "buy");                     // Options: "buy", "sell", "buyToCover", "sellShort"
        orderParams.put("orderQuantity",    String.valueOf(quantity));  // Number of shares to trade
        orderParams.put("orderSymbol",      symbol);                    // Ticker symbol for the equity or ETF 
        orderParams.put("orderPriceType",   "limit");                   // Options: "market", "limit", "stopLimit", "stopMarket"
        orderParams.put("orderExpiration",  "gtc");                     // "day" or "gtc"
        orderParams.put("orderLimitPrice",  String.valueOf(price));     // Only required if orderPriceType is limit or stopLimit
        //orderParams.put("orderStopPrice", "");                        // Only required if orderPriceType is stopMarket or stopLimit
        
        final TradePreviewTask previewTask = new TradePreviewTask();
        Callable done = new Callable() {
            @Override
            public void call(Object... params) {
                boolean success = (Boolean) params[0];
                if (success) {
                    TradePreviewResponse preview = (TradePreviewResponse) params[1];
                    HashMap<String, String> orderParams = (HashMap) params[2];
                    //
                    // TODO: We have order preview. Log or display to user warnings, etc.
                    //
                    System.out.println("Trade preview was successful: "+preview.status+". Submitting the trade...");
                    TradeTask trade = new TradeTask(new Callable(){
                        @Override
                        public void call(Object... params) {
                            boolean success = (Boolean) params[0];
                            if (success) {
                                TradeResponse trade = (TradeResponse) params[1];
                                HashMap<String, String> orderParams = (HashMap) params[2];
                                
                                System.out.println("Trade was successful: "+trade.status+". Re-query the account to get the current state.");
                                if (callback != null)
                                    callback.call(Boolean.TRUE, params[1]);                                
                                
                            } else {
                                GenericResponse error = (GenericResponse) params[1];
                                if (!TradeIt.this.handleError(Integer.parseInt(error.code), previewTask)) {
                                    logger.log("TradePreviewTask failed.");
                                    if (callback != null) 
                                        callback.call(Boolean.FALSE, params[1]);
                                }
                            }
                        }
                    });                    
                    trade.execute(authentication, preview.orderId);
                } else {
                    logger.log("TradePreviewTask failed.");
                    if (callback != null) 
                        callback.call(Boolean.FALSE, params[1]);
                }
            }
        };
        previewTask.setCallback(done);
        previewTask.execute(orderParams);
    }

    public void sellLimit(String symbol, double price, int quantity, final Callable callback) {
        logger.log("TradeIt -> sellLimit");
        
        if (!isAuthorized()) {
            callback.call(Boolean.FALSE, Error.NOT_AUTHORIZED);
            return;
        }
        
        if (!isAuthenticated()) {
            callback.call(Boolean.FALSE, Error.NOT_AUTHENTICATED);
            return;
        }
        
        final HashMap<String,String> orderParams = new HashMap<String, String>();
        orderParams.put("apiKey", isQA ? QA_KEY : PROD_KEY);
        orderParams.put("token",            authentication.getToken());
        orderParams.put("accountNumber",    accountNumber);
        orderParams.put("orderAction",      "sell");                     // Options: "buy", "sell", "buyToCover", "sellShort"
        orderParams.put("orderQuantity",    String.valueOf(quantity));  // Number of shares to trade
        orderParams.put("orderSymbol",      symbol);                    // Ticker symbol for the equity or ETF 
        orderParams.put("orderPriceType",   "limit");                   // Options: "market", "limit", "stopLimit", "stopMarket"
        orderParams.put("orderExpiration",  "gtc");                     // "day" or "gtc"
        orderParams.put("orderLimitPrice",  String.valueOf(price));     // Only required if orderPriceType is limit or stopLimit
        //orderParams.put("orderStopPrice", "");                        // Only required if orderPriceType is stopMarket or stopLimit
        
        final TradePreviewTask previewTask = new TradePreviewTask();        
        Callable done = new Callable() {
            @Override
            public void call(Object... params) {
                boolean success = (Boolean) params[0];
                if (success) {
                    TradePreviewResponse preview = (TradePreviewResponse) params[1];
                    HashMap<String, String> orderParams = (HashMap) params[2];
                    //
                    // TODO: We have order preview. Log or display to user warnings, etc.
                    //
                    System.out.println("Trade preview was successful: "+preview.status+". Submitting the trade...");
                    TradeTask trade = new TradeTask(new Callable(){
                        @Override
                        public void call(Object... params) {
                            boolean success = (Boolean) params[0];
                            if (success) {
                                TradeResponse trade = (TradeResponse) params[1];
                                HashMap<String, String> orderParams = (HashMap) params[2];                                
                                System.out.println("Trade was successful: "+trade.status+". Re-query the account to get the current state.");
                                if (callback != null)
                                    callback.call(Boolean.TRUE, params[1]);
                                
                            } else {
                                GenericResponse error = (GenericResponse) params[1];
                                if (!TradeIt.this.handleError(Integer.parseInt(error.code), previewTask)) {
                                    logger.log("TradePreviewTask failed.");
                                    if (callback != null) 
                                        callback.call(Boolean.FALSE, params[1]);
                                }
                            }
                        }
                    });                    
                    trade.execute(authentication, preview.orderId);
                } else {
                    GenericResponse error = (GenericResponse) params[1];
                    if (!TradeIt.this.handleError(Integer.parseInt(error.code), previewTask)) {                        
                        logger.log("TradePreviewTask failed.");
                        if (callback != null)
                            callback.call(Boolean.FALSE, params[1]);
                    }
                }
            }
        };
        
        previewTask.setCallback(done);
        previewTask.execute(orderParams);
    }
    
    public boolean isAuthorized() {
        return authorization != null && 
                "SUCCESS".equals(authorization.status.toUpperCase()) &&
                authorization.token != null && !authorization.token.isEmpty() && 
                authorization.userToken != null && !authorization.userToken.isEmpty();
    }
    
    public boolean isAuthenticated() {
        return authentication != null && 
                "SUCCESS".equals(authentication.status.toUpperCase()) &&
                authentication.token != null && !authentication.token.isEmpty() && 
                /*yes, we need to have authorization before authentication*/
                authorization.userToken != null && !authorization.userToken.isEmpty();
    }
    
    public List<Account> getAccounts() {
        List<Account> tradeables = new ArrayList<Account>();
        if (isAuthenticated()) {
            for (Account acct : authentication.accounts) {
                if (acct.isTradable())
                    tradeables.add(acct);
            }
            return tradeables;
        }
        return null;
    }
    
    public AccountInfoResponse getLinkedAccountInfo() {
        return accountInfo;
    }
    
    public void session() {
        session(null);
    }
    
    public void session(final Callable callback) {
        logger.log(" ** Authorize called on "+getName());
        Request req = RequestFactory.getOAuthRequest();
        req.make(
                /*success response*/
                new Callable()
                {
                    @Override
                    public void call(Object... params) {
                        if (params[0] instanceof AuthorizeResponse) {
                            authorization = (AuthorizeResponse) params[0];
                            logger.log("Success AuthorizeResponse: ");
                            logger.log("         status: " + authorization.status); 
                            logger.log("          token: " + authorization.token);
                            logger.log("      userToken: " + authorization.userToken);
                            logger.log("   shortMessage: " + authorization.shortMessage); 
                            
                            if (authorization.longMessages != null && authorization.longMessages.size()>0)
                                logger.log("    longMessage: " + authorization.longMessages.get(0));
                            
                        }
                        if (callback != null) {
                            callback.call(params);
                        }
                    }
                }, 
                /*error response*/
                new Callable()
                {
                    @Override
                    public void call(Object... params) {
                        if (params[0] instanceof AuthorizeResponse) {
                            AuthorizeResponse failauth = (AuthorizeResponse) params[0];
                            /*
                            Code    Meaning
                            100     System Error
                            200     Broker Execution Error - User should modify the input for the trade request
                            300     Broker Authentication Error - Authentication info is incorrect or the user may have changed their login information and the oAuth token is no longer valid.
                            400     Broker Account Error - User credentials are valid, but needs to take action on the brokers site (ie. sign exchange agreement, sign margin agreement)
                            500     Params Error - Publisher should check the parameters being passed in
                            600     Session Expired - Publisher should call authenticate again in order to generate a new session token
                            700     Token invalid or expired - Publisher should call oAuthUpdate in order to refresh the token
                             */
                            logger.log("Fail AuthorizeResponse: ");
                            logger.log("        status: " + failauth.status); 
                            logger.log("  shortMessage: " + failauth.shortMessage); 
                            if (failauth.longMessages != null && failauth.longMessages.size()>0)
                                logger.log("    longMessage: " + failauth.longMessages.get(0));
                        }
                        if (callback != null) {
                            callback.call(params);
                        }
                    }
                }, AuthorizeResponse.class);
    }
    
    public void authenticate() {
        authenticate(null);
    }
    
    public void authenticate(final Callable callback) {
        if (isAuthorized()) {
            logger.log(" ** Authorize called on "+getName());
            Request req = RequestFactory.getAuthenticateRequest(authorization);
            Callable success = new Callable()
            {
                @Override
                public void call(Object... params) {
                    if (params[0] != null && params[0] instanceof AuthenticateResponse) {
                        authentication = (AuthenticateResponse) params[0];
                        logger.log("Success AuthenticateResponse: ");
                        logger.log("         status: " + authentication.status); 
                        logger.log("          token: " + authentication.token); 
                        logger.log("  accountNumber: " + authentication.accountNumber); 
                        logger.log("       accounts: " + authentication.accounts.size()); 
                        if (authentication.accounts.size() > 0) {
                            for (Account account : authentication.accounts) {
                                logger.log("             name: " + account.getName()+", number="+account.getAccountNumber()+", currrency="+account.getAccountBaseCurrency()+", tradable: "+account.isTradable()); 
                            }
                        }
                        logger.log("   shortMessage: " + authentication.shortMessage); 
                        if (authentication.longMessages != null && authentication.longMessages.size()>0)
                            logger.log("    longMessage: " + authentication.longMessages.get(0));
                        
                        if (callback != null) {
                            callback.call(authentication);
                        }
                    }
                }
            };
            Callable failure = new Callable()
                {
                    @Override
                    public void call(Object... params) {
                        if (params[0] != null && params[0] instanceof AuthenticateResponse) {
                            AuthenticateResponse failauth = (AuthenticateResponse) params[0];
                            logger.log("Fail AuthenticateResponse: ");
                            logger.log("        status: " + failauth.status); 
                            logger.log("          code: " + failauth.code); 
                            logger.log("  shortMessage: " + failauth.shortMessage); 
                            if (failauth.longMessages != null && failauth.longMessages.size()>0)
                                logger.log("    longMessage: " + failauth.longMessages.get(0));
                            int code = Integer.parseInt(failauth.code);
                            handleError(code);
                        } else {
                            if (params.length > 1 && params[1] != null) {
                                String message = (String) params[1];
                                logger.log("Error: " + message);
                            }
                        } 
                    }
                };
            req.make(success, failure, AuthenticateResponse.class);
        } else {
            logger.log(Error.NOT_AUTHORIZED);
        }
    }
    
    /**
     * Handles action triggered by a specific error code.
     * Code	Meaning
        100	System Error
        200	Broker Execution Error - User should modify the input for the trade request
        300	Broker Authentication Error - Authentication info is incorrect or the user may have changed their login information and the oAuth token is no longer valid.
        400	Broker Account Error - User credentials are valid, but needs to take action on the brokers site (ie. sign exchange agreement, sign margin agreement)
        500	Params Error - Publisher should check the parameters being passed in
        600	Session Expired - Publisher should call authenticate again in order to generate a new session token
        700	Token invalid or expired - Publisher should call oAuthUpdate in order to refresh the token
     * @param code 
     */
    public void handleError(int code) {
        handleError(code, null);
    }
    
    public boolean handleError(int code, ITask andRun) {
        logger.log("[handleError] code="+code+", task="+andRun);
        switch(code) {
            case 700: //oAuthUpdate required
                oAuthUpdate();
                return true;
                
            case 600: //re-authenticate required
                authenticate(andRun);
                return true;
                
            default:
                logger.log("Error code="+code+" not handled.");
        }
        return false;
    }
    
    /**
     * A call to update the userToken as a result of a failed authentication with code 700.
     */
    public void oAuthUpdate() {
        oAuthUpdate(null);
    }
    public void oAuthUpdate(final Callable callback) {
        logger.log(" ** oAuthUpdate called on "+getName());
        Request req = RequestFactory.getOAuthUpdateRequest(authorization);
        req.make(
                /*success response*/
                new Callable()
                {
                    @Override
                    public void call(Object... params) {
                        if (params[0] instanceof AuthorizeResponse) {
                            AuthorizeResponse auth = (AuthorizeResponse) params[0];
                            logger.log("Success AuthenticateResponse: ");
                            logger.log("              status: " + auth.status); 
                            logger.log("   updated userToken: " + auth.userToken);
                            authorization.userToken = auth.userToken;
                            authorization.token = auth.token;
                            authenticate();
                        }
                        if (callback != null)
                            callback.call(Boolean.TRUE, params[0]);
                    }
                }, 
                /*error response*/
                new Callable()
                {
                    @Override
                    public void call(Object... params) {
                        if (params[0] instanceof AuthorizeResponse) {
                            AuthorizeResponse failauth = (AuthorizeResponse) params[0];
                            /*
                            Code    Meaning
                            100     System Error
                            200     Broker Execution Error - User should modify the input for the trade request
                            300     Broker Authentication Error - Authentication info is incorrect or the user may have changed their login information and the oAuth token is no longer valid.
                            400     Broker Account Error - User credentials are valid, but needs to take action on the brokers site (ie. sign exchange agreement, sign margin agreement)
                            500     Params Error - Publisher should check the parameters being passed in
                            600     Session Expired - Publisher should call authenticate again in order to generate a new session token
                            700     Token invalid or expired - Publisher should call oAuthUpdate in order to refresh the token
                             */
                            logger.log("Fail AuthorizeResponse: ");
                            logger.log("        status: " + failauth.status); 
                            logger.log("  shortMessage: " + failauth.shortMessage); 
                            if (failauth.longMessages != null && failauth.longMessages.size()>0)
                                logger.log("    longMessage: " + failauth.longMessages.get(0));
                        }
                        if (callback != null)
                            callback.call(Boolean.FALSE, params[0]);
                    }
                }, AuthorizeResponse.class);
    }
    
    /**
     * Collect account info.
     */
    public void accountInfo(String accountNum) {
        accountInfo(accountNum, null);
    }
    
    public void accountInfo(final String accountNum, final Callable callback) {
        
        if (!isAuthorized()) {
            if (callback != null)
                callback.call(Boolean.FALSE, Error.NOT_AUTHORIZED);
            return;
        }
        
        if (!isAuthenticated()) {
            if (callback != null)
                callback.call(Boolean.FALSE, Error.NOT_AUTHENTICATED);
            return;
        }
            
        final AccountInfoTask task = new AccountInfoTask();        
        Callable done = new Callable() {
            @Override
            public void call(Object... params) 
            {
                boolean issuccess = (Boolean) params[0];
                Object result = params[1];
                if (issuccess) 
                {
                    if (result instanceof AccountInfoResponse) {
                        accountInfo = (AccountInfoResponse) result;
                        logger.log("Success AccountInfoResponse: ");
                        logger.log("         totalValue: " + accountInfo.totalValue); 
                        logger.log("      availableCash: " + accountInfo.availableCash);
                        logger.log("   dayPercentReturn: " + accountInfo.dayPercentReturn);
                        TradeIt.this.accountNumber = accountNum;
                    }
                    if (callback != null)
                        callback.call(issuccess, result);
                } 
                else if (result instanceof GenericResponse) 
                {
                    GenericResponse error = (GenericResponse) params[0];
                    if (params[1] instanceof AccountInfoResponse) 
                    {
                        AccountInfoResponse failacct = (AccountInfoResponse) result;                            
                        logger.log("Fail AccountInfoResponse: ");
                        logger.log("        status: " + failacct.status); 
                        logger.log("  shortMessage: " + failacct.shortMessage); 
                        if (failacct.longMessages != null && failacct.longMessages.size()>0)
                            logger.log("    longMessage: " + failacct.longMessages.get(0));
                    }
                    if (!TradeIt.this.handleError(Integer.parseInt(error.code), task)) {
                        if (callback != null) 
                            callback.call(Boolean.FALSE, params[1]);
                    }
                }
            }
        };
        task.setCallback(done);
        task.execute(authentication, accountNum);
    }
    
    public void positions(final String accountNumber, int page, final Callable callback) {
        
        if (!isAuthorized()) {
            if (callback != null)
                callback.call(Boolean.FALSE, Error.NOT_AUTHORIZED);
            return;
        }
        
        if (!isAuthenticated()) {
            if (callback != null)
                callback.call(Boolean.FALSE, Error.NOT_AUTHENTICATED);
            return;
        }
        
        final GetPositionsTask task = new GetPositionsTask();        
        Callable done = new Callable() {
            @Override
            public void call(Object... params) {
                boolean isSuccess = (Boolean) params[0];
                if (isSuccess) {
                if (callback != null)
                    callback.call(Boolean.TRUE, (params.length > 1 ? params[1] : null));
                } else {
                    GenericResponse error = (GenericResponse) params[1];
                    if (!TradeIt.this.handleError(Integer.parseInt(error.code), task)) {
                        if (callback != null) 
                            callback.call(Boolean.FALSE, params[1]);
                    }
                }
            }
        };
        
        task.setCallback(done);
        task.execute(authentication, accountNumber, page);
    }
    
}
