/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit;

import com.gorny.trading.model.response.GenericResponse;
import com.gorny.trading.model.response.TradePreviewResponse;
import com.gorny.trading.Callable;
import com.gorny.trading.Utils;

/**
 *
 * @author maciejg
 */
public class TradeItRequest<T> extends Request<T> {
    
    @Override
    public void make(final Callable success, final Callable error, final Class<T> responseType) {
        httpPost(new Callable() {
            @Override
            public void call(Object... response) {
                if (response != null) {
                    
                    String responseCode = (String) response[0];
                    String responseBody = (String) response[1];
                    
                    if (Request.SUCCESS.equals(responseCode)) {
                        
                        Object obj = responseType.cast(Utils.fromJson(responseBody, responseType));                                               
                        if (obj != null) {
                            
                            if (obj instanceof TradePreviewResponse) {
                                TradePreviewResponse preview = (TradePreviewResponse) obj;
                                if ("REVIEW_ORDER".equals(preview.status) && !preview.orderId.isEmpty()) 
                                    success.call(preview);
                                else 
                                    error.call(preview);
                            } 
                            else if (obj instanceof GenericResponse) {
                                
                                GenericResponse result = (GenericResponse) obj;
                                if ("success".equals(result.status.toLowerCase()))
                                    success.call(result);
                                
                                else if ("error".equals(result.status.toLowerCase()))
                                    error.call(result);
                            }                    
                        }
                    } else if (error != null) {                        
                        error.call(null, responseBody);
                    } 
                } else {
                    System.out.println("Null response received");
                }
            }
        });
    }

}
