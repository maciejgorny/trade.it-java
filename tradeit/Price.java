/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit;

/**
 *
 * @author maciejg
 */
public class Price {
    
    /**
     * "market","limit","stopLimit","stopMarket"
     */
    public String type;
    
    /**
     * Limit price if limit order or stop limit order
     */
    public double limitPrice;
    
    /**
     * Stop price if stop market or stop limit order
     */
    public double stopPrice;
    
    /**
     * Real-time bid price of security, at time of review
     */
    public double bid;
    
    /**
     * Real-time ask price of security, at time of review
     */
    public double ask;
    
    /**
     * Real-time last traded price of security, at time of review
     */
    public double last;        
    
    /**
     * Real-time quote last updated at time, at time of review
     */
    public String timestamp;
}
