/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit;

/**
 *
 * @author maciejg
 */
public class Position {

/**
 * the total base cost of the security
 */
public double costbasis;

/**
 * String, "LONG" or "SHORT"
 */
public String holdingType;

/**
 * the last traded price of the security
 */
public double lastPrice;

/**
 * the total quantity held. It's a double to support cash and Mutual Funds
 */
public double quantity; 

/**
 * the ticker symbol or CUSIP for fixed income positions. Symbols for Class A or B shares use dot annotation (i.e BRK.A)
 */
public String symbol;

/**
 * the type of security: EQUITY_OR_ETF, MUTUAL_FUND, OPTION, FIXED_INCOME, CASH, UNKOWN
 */
public String symbolClass;

/**
 * Double, the total gain/loss in dollars for the day for the position
 */
public double todayGainLossDollar;

/**
 * Double, the percentage gain/loss for the day for the position
 */
public double todayGainLossPercentage;

/**
 * Double, the total gain/loss in dollars for the position
 */
public double totalGainLossDollar;

/**
 * Double, the total percentage of gain/loss for the position
 */
public double totalGainLossPercentage;
}
