/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model.tradeit;

/**
 *
 * @author maciejg
 */
public class OrderDetails {
    public String orderId;          //	TradeIt internal order id, used with place order request
    public String orderSymbol;      //	The street symbol for the trade
    public String orderAction;      //	The order action used for the trade "buy", "sell", "sellShort", "buyToCover"
    public Double orderQuantity;    //	The quantity of the security to be traded
    public String orderExpiration;  //	"day" or "gtc"
    public String orderPrice;       //	"market" for market orders, limit/stop will show the trigger price, stopLimit shows the trigger and limit
    public String orderValueLabel;  //	"Estimated Proceeds" or "Estimated Cost"
    public String orderMessage;     //	Top level readable review of order ie. "You are about to place a market order to buy EROS"
    
    public Double lastPrice;        //	Real-time last traded price of security
    public Double bidPrice;         //	Real-time bid price of security
    public Double askPrice;         //	Real-time ask price of security
    
    public String timestamp;        //	String, the time the real-time quote was last updated
    public Double buyingPower;      //	The current buying power of the account
    public Double longHoldings;     //	The currently long held quantity of the security to be traded
    public Double shortHoldings;    //	The currently short held quantity of the security to be traded
    public Double availableCash;    //	Cash available to trade in the account
    
    public Double estimatedOrderCommission; //	Estimated broker commission for the order
    public Double estimatedOrderValue;      //	Estimated value of the order, does not include fees
    public Double estimatedTotalValue;      //	Estimated total cost of the order including fees
}
