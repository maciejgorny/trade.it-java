/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gorny.trading.model;

import com.gorny.trading.Callable;

/**
 *
 * @author maciejg
 */
public abstract class TaskImpl implements ITask {
    
    protected Callable finishedCallback = null;
    
    public TaskImpl() {    
    }
    
    public TaskImpl(Callable done) {
        this.finishedCallback = done;
    }
    
    public void setCallback(Callable callback) {
        this.finishedCallback = callback;
    }
    
    @Override
    public void call(Object... params) {
        execute(params);
    }
    
    @Override
    public abstract void execute(Object... params);
    
}
